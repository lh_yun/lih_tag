# lih_tag

#### 介绍
标签
参考 

https://element.eleme.cn/#/zh-CN/component/tag

https://www.layui.com/demo/tab.html

#### 软件架构
layui 前端框架
https://www.layui.com/


#### 使用说明
![![输入图片说明](https://images.gitee.com/uploads/images/2019/0916/011958_d5fe1be1_1909409.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2019/0916/011958_9a364a4b_1909409.png "屏幕截图.png")
示例
http://lh_yun.gitee.io/lih_tag/

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)